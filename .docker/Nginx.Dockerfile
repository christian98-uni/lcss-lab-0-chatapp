FROM nginx:1.25-alpine as builder

WORKDIR /var/www

# Add Source-Code
COPY public /var/www/public

# Configure Nginx
COPY .docker/nginx/nginx.conf /etc/nginx/nginx.conf

# Set specific permissions
RUN rm -R -f .git \
    && find . -type d -exec chmod 0755 {} \; \
    && find . -type f -exec chmod 0644 {} \;

FROM nginx:1.25-alpine

WORKDIR /var/www

COPY --from=builder /var/www/public /var/www/public
COPY --from=builder /etc/nginx/nginx.conf /etc/nginx/nginx.conf

HEALTHCHECK --timeout=15s \
  CMD wget -q -O /dev/null http://localhost/health
