<?php

namespace App\Models;

use App\Events\ChatCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Chat extends Model
{
    protected $fillable = [
        'name',
    ];

    protected static function booted(): void
    {
        parent::booted();

        self::created(function (Chat $chat) {
            broadcast((new ChatCreated($chat))->dontBroadcastToCurrentUser());
        });
    }

    public function messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class);
    }
}
