<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use App\Models\Chat;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class ChatsController extends Controller
{
    public function show(Chat $chat): Response
    {
        return Inertia::render('Chat', [
            'chat' => new ChatResource($chat->loadMissing(['messages', 'messages.user'])),
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        Chat::create($data);

        return redirect()->route('dashboard');
    }
}
