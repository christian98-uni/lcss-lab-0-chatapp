<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\ChatMessage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class ChatMessagesController extends Controller
{
    public function store(Chat $chat, Request $request): RedirectResponse
    {
        $data = $request->validate([
            'content' => ['required', 'string'],
        ]);

        /** @var ChatMessage $message */
        $message = $chat->messages()->make($data);
        $message->user()->associate(Auth::user());
        $message->save();

        return redirect()->route('chats.show', ['chat' => $chat]);
    }
}
