<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use App\Models\Chat;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function __invoke()
    {
        return Inertia::render('Dashboard', [
            'chats' => ChatResource::collection(
                Chat::query()->with(['messages', 'messages.user'])->get()
            ),
        ]);
    }
}
