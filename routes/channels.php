<?php

use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('App.Models.Chat.{id}', function ($user, $id) {
    return !is_null($user);
});

Broadcast::channel('App.Models.Chat', function ($user) {
    return !is_null($user);
});
