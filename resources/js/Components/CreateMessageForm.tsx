import {ChangeEvent, FormEvent, useState} from 'react'
import {router} from "@inertiajs/react";
import {Chat, PageProps} from "@/types";

interface CreateMessageFormProps extends PageProps{
    chat: Chat,
}

export default function CreateMessageForm(props: CreateMessageFormProps) {
    const [values, setValues] = useState({
        content: '',
    })

    function handleChange(e: ChangeEvent<HTMLTextAreaElement>) {
        const key = e.target.id;
        const value = e.target.value
        setValues(values => ({
            ...values,
            [key]: value,
        }))
    }

    function handleSubmit(e: FormEvent) {
        e.preventDefault()
        router.post(`/chats/${props.chat.id}/messages`, values)
    }

    return (
        <div className="flex items-start space-x-4">
            <div className="flex-shrink-0">
                <img
                    className="inline-block h-10 w-10 rounded-full"
                    src={`https://gravatar.com/avatar/${props.auth.user.gravatar}`}
                    alt=""
                />
            </div>
            <div className="min-w-0 flex-1">
                <form onSubmit={handleSubmit} className="relative">
                    <div
                        className="overflow-hidden rounded-lg shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-indigo-600">
                        <label htmlFor="comment" className="sr-only">
                            Add your comment
                        </label>
                        <textarea
                            rows={3}
                            value={values.content}
                            onChange={handleChange}
                            name="content"
                            id="content"
                            className="block w-full resize-none border-0 bg-transparent py-1.5 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder="Add your comment..."
                        />

                        {/* Spacer element to match the height of the toolbar */}
                        <div className="py-2" aria-hidden="true">
                            {/* Matches height of button in toolbar (1px border + 36px content height) */}
                            <div className="py-px">
                                <div className="h-9"/>
                            </div>
                        </div>
                    </div>

                    <div className="absolute inset-x-0 bottom-0 flex justify-between py-2 pl-3 pr-2">
                        <div className="flex-shrink-0">
                            <button
                                type="submit"
                                className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                            >
                                Post
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
