import {ChangeEvent, FormEvent, useState} from "react";
import {router} from "@inertiajs/react";

export function CreateChatForm() {
    const [values, setValues] = useState({
        name: '',
    })

    function handleChange(e: ChangeEvent<HTMLInputElement>) {
        const key = e.target.id;
        const value = e.target.value
        setValues(values => ({
            ...values,
            [key]: value,
        }))
    }

    function handleSubmit(e: FormEvent) {
        e.preventDefault()
        router.post('/chats', values)
    }

    return (
        <div className="bg-white shadow sm:rounded-lg py-2">
            <div className="px-4 py-5">
                <h3 className="text-base font-semibold leading-6 text-gray-900">Create new Chat-Topic</h3>
                <form onSubmit={handleSubmit} className="mt-5 sm:flex sm:items-center">
                    <div className="w-full sm:max-w-xs">
                        <label htmlFor="email" className="sr-only">
                            Name
                        </label>
                        <input
                            value={values.name}
                            onChange={handleChange}
                            type="name"
                            name="name"
                            id="name"
                            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 px-2 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            placeholder="My topic"
                        />
                    </div>
                    <button
                        type="submit"
                        className="mt-3 inline-flex w-full items-center justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 sm:ml-3 sm:mt-0 sm:w-auto"
                    >
                        Create
                    </button>
                </form>
            </div>
        </div>
    )
}
