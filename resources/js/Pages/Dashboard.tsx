import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head, Link} from '@inertiajs/react';
import {Chat, PageProps} from '@/types';
import {ChatBubbleLeftIcon} from "@heroicons/react/24/outline";
import {CreateChatForm} from "@/Components/CreateChatForm";
import {useEffect, useState} from "react";
import {echo} from "@/echo";

interface DashboardProps extends PageProps {
    chats: Chat[];
}

export default function Dashboard(props: DashboardProps) {
    const [chats, setChats] = useState(props.chats);

    useEffect(() => {
        setChats(props.chats);
    }, [props.chats.length]);

    useEffect(() => {
        const channel = echo.private(`App.Models.Chat`)
            .listen('ChatCreated', (e: { chat: Chat }) => {
                setChats((prevState) => [...prevState, e.chat]);
            });

        return () => {
            channel.stopListening(`App.Models.Chat`);
        }
    }, []);

    return (
        <AuthenticatedLayout
            user={props.auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard"/>

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-4">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900">Hi, {props.auth.user.name}!</div>
                    </div>

                    <CreateChatForm/>

                    <div>
                        <ul role="list" className="divide-y divide-gray-100">
                            {chats.map((chat) => (
                                <li key={chat.id}>
                                    <Link href={`/chats/${chat.id}`}
                                          className="flex rounded-lg hover:bg-white flex-wrap items-center justify-between gap-x-6 gap-y-4 py-5 sm:flex-nowrap">
                                        <div>
                                            <p className="text-sm font-semibold leading-6 pl-4 text-gray-900">
                                                {chat.name}
                                            </p>
                                        </div>
                                        <dl className="flex w-full flex-none justify-between gap-x-8 sm:w-auto">
                                            <div className="flex -space-x-0.5">
                                                <dt className="sr-only">Commenters</dt>
                                                {[...new Map(chat.messages.map(message =>
                                                    [message.user.id, message.user])).values()].map((commenter) => (
                                                    <dd key={commenter.id}>
                                                        <img
                                                            className="h-6 w-6 rounded-full bg-gray-50 ring-2 ring-white"
                                                            src={`https://gravatar.com/avatar/${commenter.gravatar}`}
                                                            alt={commenter.name}
                                                        />
                                                    </dd>
                                                ))}
                                            </div>
                                            <div className="flex w-16 gap-x-2.5">
                                                <dt>
                                                    <span className="sr-only">Total comments</span>
                                                    <ChatBubbleLeftIcon className="h-6 w-6 text-gray-400"
                                                                        aria-hidden="true"/>
                                                </dt>
                                                <dd className="text-sm leading-6 text-gray-900">{chat.messages.length}</dd>
                                            </div>
                                        </dl>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
