import CreateMessageForm from "@/Components/CreateMessageForm";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import {Chat as ChatData, Message, PageProps} from "@/types";
import {Head, Link} from "@inertiajs/react";
import {ArrowLeftIcon} from "@heroicons/react/24/outline";
import {useEffect, useState} from "react";
import {echo} from "@/echo";

interface ChatProps extends PageProps {
    chat: ChatData,
}

export default function Chat(props: ChatProps) {
    const [messages, setMessages] = useState(props.chat.messages);

    useEffect(() => {
        setMessages(props.chat.messages);
    }, [props.chat.messages.length]);

    useEffect(() => {
        const channel = echo.private(`App.Models.Chat.${props.chat.id}`)
            .listen('MessageSent', (e: { message: Message }) => {
                setMessages((prevState) => [...prevState, e.message]);
            });

        return () => {
            channel.stopListening(`App.Models.Chat.${props.chat.id}`);
        }
    }, [props.chat.id]);

    return (
        <AuthenticatedLayout
            user={props.auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{props.chat.name}</h2>}
        >
            <Head title={props.chat.name}/>

            <div className="flex-col flex">
                <div className="px-12 my-4">
                    <Link href="/dashboard" className="flex items-center text-indigo-500">
                        <ArrowLeftIcon className="h-5 w-5 mr-1"/>
                        Back To Dashboard
                    </Link>
                </div>

                <div className="pb-8 px-12 shrink">
                    <ul role="list" className="divide-y divide-gray-100">
                        {messages.map((message) => (
                            <li key={message.id} className="flex gap-x-4 py-5">
                                <img className="h-12 w-12 flex-none rounded-full bg-gray-50"
                                     src={`https://gravatar.com/avatar/${message.user.gravatar}`}
                                     alt={message.user.name}/>
                                <div className="flex-auto">
                                    <div className="flex items-baseline justify-between gap-x-4">
                                        <p className="text-sm font-semibold leading-6 text-gray-900">{message.user.name}</p>
                                        <p className="flex-none text-xs text-gray-600">
                                            <time dateTime={message.created_at}>
                                                {new Intl.DateTimeFormat('en',{
                                                    hourCycle: 'h24',
                                                    dateStyle: 'medium',
                                                    timeStyle: 'medium',
                                                }).format(new Date(message.created_at))}
                                            </time>
                                        </p>
                                    </div>
                                    <p className="mt-1 line-clamp-2 text-sm leading-6 text-gray-600">{message.content}</p>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>

                <div className="px-12">
                    <CreateMessageForm auth={props.auth} chat={props.chat}/>
                </div>

                <div className="px-12 my-4">
                    <Link href="/dashboard" className="flex items-center text-indigo-500">
                        <ArrowLeftIcon className="h-5 w-5 mr-1"/>
                        Back To Dashboard
                    </Link>
                </div>
            </div>
        </AuthenticatedLayout>
    )
}
