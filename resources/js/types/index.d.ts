export interface User {
    id: number;
    name: string;
    email: string;
    email_verified_at: string;
    gravatar: string;
}

export interface Chat {
    id: string;
    name: string;
    messages: Message[],
}

export interface Message {
    id: string;
    content: string;
    user: User,
    created_at: string;
}

export type PageProps<T extends Record<string, unknown> = Record<string, unknown>> = T & {
    auth: {
        user: User;
    };
};
